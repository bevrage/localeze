CREATE TABLE IF NOT EXISTS localeze.annualsalesamountcode
(
	annualsalesamountcodeid VARCHAR(2) NOT NULL  ENCODE lzo
	,description VARCHAR(25) NOT NULL  ENCODE lzo
)

;

CREATE TABLE IF NOT EXISTS localeze.baserecords
(
	pid INTEGER NOT NULL  ENCODE az64
	,chainid INTEGER   ENCODE az64
	,pubdate DATE   ENCODE az64
	,businessname VARCHAR(100)   ENCODE lzo
	,stdname VARCHAR(100)   ENCODE lzo
	,subdepartment VARCHAR(100)   ENCODE lzo
	,housenumber VARCHAR(10)   ENCODE lzo
	,predirectional VARCHAR(2)   ENCODE lzo
	,streetname VARCHAR(28)   ENCODE lzo
	,streettype VARCHAR(4)   ENCODE lzo
	,postdirectional VARCHAR(2)   ENCODE lzo
	,apttype VARCHAR(4)   ENCODE lzo
	,aptnumber VARCHAR(8)   ENCODE lzo
	,exppubcity VARCHAR(28)   ENCODE lzo
	,city VARCHAR(28)   ENCODE lzo
	,state VARCHAR(2)   ENCODE lzo
	,zip VARCHAR(5)   ENCODE lzo
	,plus4 VARCHAR(4)   ENCODE lzo
	,dpc VARCHAR(3)   ENCODE lzo
	,carrierroute VARCHAR(4)   ENCODE lzo
	,statefips VARCHAR(2)   ENCODE lzo
	,countyfips VARCHAR(3)   ENCODE lzo
	,z4type CHAR(1)   ENCODE lzo
	,censustract VARCHAR(6)   ENCODE lzo
	,censusblockgroup CHAR(1)   ENCODE lzo
	,censusblockid VARCHAR(3)   ENCODE lzo
	,msa VARCHAR(4)   ENCODE lzo
	,cbsa VARCHAR(5)   ENCODE lzo
	,mcd VARCHAR(5)   ENCODE lzo
	,addresssensitivity CHAR(1)   ENCODE lzo
	,genrc CHAR(1)   ENCODE lzo
	,mlsc CHAR(1)   ENCODE lzo
	,goldflag CHAR(1)   ENCODE lzo
	,dpvconfirm VARCHAR(1)   ENCODE lzo
	,areacode VARCHAR(3)   ENCODE lzo
	,exchange VARCHAR(3)   ENCODE lzo
	,phonenumber VARCHAR(4)   ENCODE lzo
	,dnc CHAR(1)   ENCODE lzo
	,dso CHAR(1)   ENCODE lzo
	,timezone CHAR(1)   ENCODE lzo
	,valflag CHAR(1)   ENCODE lzo
	,valdate DATE   ENCODE az64
	,valflag2 CHAR(1)   ENCODE lzo
	,confidencescore INTEGER   ENCODE az64
	,llmatchlevel CHAR(1)   ENCODE lzo
	,latitude VARCHAR(11)   ENCODE lzo
	,longitude VARCHAR(11)   ENCODE lzo
	,firstyear INTEGER   ENCODE az64
	,stdhours VARCHAR(88)   ENCODE lzo
	,hoursopen VARCHAR(100)   ENCODE lzo
	,tagline VARCHAR(100)   ENCODE lzo
	,filler53 VARCHAR(100)   ENCODE lzo
	,filler54 VARCHAR(100)   ENCODE lzo
	,filler55 VARCHAR(100)   ENCODE lzo
	,filler56 VARCHAR(20)   ENCODE lzo
	,filler57 VARCHAR(4)   ENCODE lzo
	,filler58 CHAR(1)   ENCODE lzo
	,filler59 VARCHAR(8)   ENCODE lzo
	,filler60 CHAR(1)   ENCODE lzo
	,filler61 VARCHAR(8)   ENCODE lzo
	,filler62 CHAR(1)   ENCODE lzo
	,filler63 VARCHAR(8)   ENCODE lzo
	,filler64 CHAR(1)   ENCODE lzo
	,filler65 CHAR(1)   ENCODE lzo
	,PRIMARY KEY (pid)
)

;

CREATE TABLE IF NOT EXISTS localeze.categories
(
	categoryid INTEGER NOT NULL  ENCODE az64
	,categoryname VARCHAR(100) NOT NULL  ENCODE lzo
	,PRIMARY KEY (categoryid)
)

;

CREATE TABLE IF NOT EXISTS localeze.chains
(
	chainid INTEGER NOT NULL  ENCODE az64
	,stdname VARCHAR(100)   ENCODE lzo
	,PRIMARY KEY (chainid)
)

;

CREATE TABLE IF NOT EXISTS localeze.companyattributes
(
	pid INTEGER NOT NULL  ENCODE az64
	,attributeid INTEGER NOT NULL  ENCODE az64
	,attributename VARCHAR(100) NOT NULL  ENCODE lzo
	,groupname VARCHAR(100) NOT NULL  ENCODE lzo
	,grouptype INTEGER NOT NULL  ENCODE az64
	,categoryid INTEGER NOT NULL  ENCODE az64
	,PRIMARY KEY (pid, attributeid, groupname)
)

;

CREATE TABLE IF NOT EXISTS localeze.companyheadings
(
	pid INTEGER NOT NULL  ENCODE az64
	,normalizedid INTEGER NOT NULL  ENCODE az64
	,condensedid INTEGER NOT NULL  ENCODE az64
	,categoryid INTEGER NOT NULL  ENCODE az64
	,relevancy INTEGER NOT NULL  ENCODE az64
	,PRIMARY KEY (pid, normalizedid)
)

;

CREATE TABLE IF NOT EXISTS localeze.companylanguages
(
	pid INTEGER NOT NULL  ENCODE az64
	,languageid INTEGER NOT NULL  ENCODE az64
	,PRIMARY KEY (pid, languageid)
)

;

CREATE TABLE IF NOT EXISTS localeze.companypaymenttypes
(
	pid INTEGER NOT NULL  ENCODE az64
	,paymenttypeid INTEGER NOT NULL  ENCODE az64
	,PRIMARY KEY (pid, paymenttypeid)
)

;

CREATE TABLE IF NOT EXISTS localeze.companyphones
(
	pid INTEGER NOT NULL  ENCODE az64
	,areacode CHAR(3) NOT NULL  ENCODE lzo
	,exchange CHAR(3) NOT NULL  ENCODE lzo
	,phonenumber CHAR(4) NOT NULL  ENCODE lzo
	,phonetype CHAR(1) NOT NULL  ENCODE lzo
	,valflag CHAR(1)   ENCODE lzo
	,valdate DATE   ENCODE az64
	,dnc CHAR(1)   ENCODE lzo
	,PRIMARY KEY (pid, areacode, exchange, phonenumber, phonetype)
)

;

CREATE TABLE IF NOT EXISTS localeze.companysic
(
	pid INTEGER NOT NULL  ENCODE az64
	,sic CHAR(8) NOT NULL  ENCODE lzo
	,relevancy INTEGER NOT NULL  ENCODE az64
	,PRIMARY KEY (pid, sic)
)

;

CREATE TABLE IF NOT EXISTS localeze.companyunstructured
(
	pid INTEGER NOT NULL  ENCODE az64
	,attribute VARCHAR(100) NOT NULL  ENCODE lzo
	,relevancy INTEGER NOT NULL  ENCODE az64
	,PRIMARY KEY (pid, attribute)
)

;

CREATE TABLE IF NOT EXISTS localeze.condensedheadingdetail
(
	condensedid INTEGER NOT NULL  ENCODE az64
	,condensedname VARCHAR(100) NOT NULL  ENCODE lzo
	,PRIMARY KEY (condensedid)
)

;

CREATE TABLE IF NOT EXISTS localeze.customattributes
(
	pid INTEGER NOT NULL  ENCODE az64
	,attribute VARCHAR(200) NOT NULL  ENCODE lzo
	,attributetype INTEGER NOT NULL  ENCODE az64
	,description VARCHAR(100)   ENCODE lzo
	,relevancy INTEGER NOT NULL  ENCODE az64
	,PRIMARY KEY (pid, attribute, attributetype)
)

;

CREATE TABLE IF NOT EXISTS localeze.customattributestypes
(
	attributetypeid INTEGER NOT NULL  ENCODE az64
	,attributetype VARCHAR(100) NOT NULL  ENCODE lzo
	,PRIMARY KEY (attributetypeid)
)

;

CREATE TABLE IF NOT EXISTS localeze.dpvconfirm
(
	dpvconfirmid VARCHAR(2) NOT NULL  ENCODE lzo
	,description VARCHAR(100) NOT NULL  ENCODE lzo
)

;

CREATE TABLE IF NOT EXISTS localeze.employeesizecode
(
	employeesizecodeid VARCHAR(2) NOT NULL  ENCODE lzo
	,description VARCHAR(25) NOT NULL  ENCODE lzo
)

;

CREATE TABLE IF NOT EXISTS localeze.firmographics
(
	persistent_id INTEGER NOT NULL  ENCODE az64
	,contactlastname VARCHAR(255) NOT NULL  ENCODE lzo
	,contactfirstname VARCHAR(255) NOT NULL  ENCODE lzo
	,contactinitial VARCHAR(50)   ENCODE lzo
	,employeesizecodeid VARCHAR(50)   ENCODE lzo
	,annualsalescodeid VARCHAR(50)   ENCODE lzo
	,yearsinbusinessid VARCHAR(50)   ENCODE lzo
)

;

CREATE TABLE IF NOT EXISTS localeze.grouptype
(
	grouptypeid INTEGER NOT NULL  ENCODE az64
	,description VARCHAR(100) NOT NULL  ENCODE lzo
)

;

CREATE TABLE IF NOT EXISTS localeze.industryclass
(
	industryclassid VARCHAR(2) NOT NULL  ENCODE lzo
	,description VARCHAR(100) NOT NULL  ENCODE lzo
)

;

CREATE TABLE IF NOT EXISTS localeze.languages
(
	languageid INTEGER NOT NULL  ENCODE az64
	,languagename VARCHAR(100) NOT NULL  ENCODE lzo
	,PRIMARY KEY (languageid)
)

;

CREATE TABLE IF NOT EXISTS localeze.llmatchlevel
(
	llmatchlevel VARCHAR(2) NOT NULL  ENCODE lzo
	,description VARCHAR(100) NOT NULL  ENCODE lzo
)

;

CREATE TABLE IF NOT EXISTS localeze.mlsc
(
	mlscid INTEGER NOT NULL  ENCODE az64
	,description VARCHAR(100) NOT NULL  ENCODE lzo
)

;

CREATE TABLE IF NOT EXISTS localeze.normalizedheadingdetail
(
	normalizedid INTEGER NOT NULL  ENCODE az64
	,normalizedname VARCHAR(100) NOT NULL  ENCODE lzo
	,PRIMARY KEY (normalizedid)
)

;

CREATE TABLE IF NOT EXISTS localeze.paymenttypes
(
	paymenttypeid INTEGER NOT NULL  ENCODE az64
	,paymenttype VARCHAR(100) NOT NULL  ENCODE lzo
	,PRIMARY KEY (paymenttypeid)
)
;

CREATE TABLE IF NOT EXISTS localeze.phonetypes
(
	phonetypesid VARCHAR(2) NOT NULL  ENCODE lzo
	,description VARCHAR(100) NOT NULL  ENCODE lzo
)
;

CREATE TABLE IF NOT EXISTS localeze.sicdetail
(
	sic CHAR(8) NOT NULL  ENCODE lzo
	,description VARCHAR(50)   ENCODE lzo
	,sic_4 CHAR(4)   ENCODE lzo
	,sic_4_description VARCHAR(50)   ENCODE lzo
	,industryclass CHAR(2)   ENCODE lzo
	,naics VARCHAR(6)   ENCODE lzo
	,naics_description VARCHAR(100)   ENCODE lzo
	,PRIMARY KEY (sic)
)

;

CREATE TABLE IF NOT EXISTS localeze.yearinbusiness
(
	yearinbusinessid VARCHAR(2) NOT NULL  ENCODE lzo
	,description VARCHAR(25) NOT NULL  ENCODE lzo
)
;
COMMIT;