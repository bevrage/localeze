import boto3
import json
import psycopg2
import utils 
from datetime import datetime

#  Getting parameters     

host = utils.host
dbname = utils.dbname 
port= utils.port 
pwd = utils.pwd 
user= utils.user 
schema = utils.schema 
now = datetime.now() 
aws_access_key_id = utils.aws_access_key_id 
aws_secret_access_key_id = utils.aws_secret_access_key_id
bucket_name= utils.bucket 
path_from = utils.path_from
path_to= utils.path_to 
aws_access_key_id= utils.aws_access_key_id   
aws_secret_access_key_id = utils.aws_secret_access_key_id  


# Connecting to s3  and redshift 

s3 = boto3.resource('s3',  aws_access_key_id= aws_access_key_id, aws_secret_access_key= aws_secret_access_key_id)

conn=psycopg2.connect(dbname= dbname,
    host= host , 
    port= port ,
    user= user , 
    password= pwd)
cur = conn.cursor()

src = s3.Bucket(bucket_name)

# uploading each file to redshift 
for file  in src.objects.filter(Prefix=path_from):
        filesource=file.key
        file = filesource.split("/pending/",1)[1]
        
        if file != "" :
            table=  file[0:-4]
            date= now.strftime("%m%d%Y")
            filedest= path_to + date + '/' + table +   '.txt' 

            query = " delete from "+ schema + "." + table ; 
            cur.execute(query)
            conn.commit() 

            query = "copy " + schema + "." + table + " from " + chr(39) + 's3://' + bucket_name + '/' + path_from  + file + chr(39)
            query = query + " credentials  " + chr(39) + 'aws_access_key_id=' + aws_access_key_id + ';aws_secret_access_key=' + aws_secret_access_key_id + chr(39)
            query = query + " ACCEPTINVCHARS " 
            query = query + " delimiter " + chr(39) + "|" + chr(39) + ";"
          
            
            cur.execute(query)
            conn.commit() 

            copy_source = {
                'Bucket': bucket_name,
                'Key': filesource  
                 }            

            s3.Object(bucket_name, filedest).copy(copy_source)

            s3.Object(bucket_name, filesource).delete()
        else : 
            print ("folder is empty")
